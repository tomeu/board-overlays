#!/bin/sh
#
# Copyright 2020 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# The major and minor versions are used to give the private chromeos-config-bsp
# precedence over the public. Thus, the major and minor versions MUST remain 2
# and 0, and only the third field may be incremented.
echo 2.0.0