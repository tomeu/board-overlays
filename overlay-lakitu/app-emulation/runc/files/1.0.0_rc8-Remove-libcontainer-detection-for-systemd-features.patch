From 27df92a0f9741b26c2f7c57a16047531edbbeed9 Mon Sep 17 00:00:00 2001
From: Filipe Brandenburger <filbranden@gmail.com>
Date: Thu, 22 Aug 2019 21:53:24 -0700
Subject: [PATCH] Remove libcontainer detection for systemd features

Transient units (and transient slice units) have been available for quite a
long time and RHEL 7 with systemd v219 (likely the oldest OS we care about at
this point) supports that. A system running a systemd without these features is
likely to break a lot of other stuff that runc/libcontainer care about.

Regarding delegated slices, modern systemd doesn't allow it and
runc/libcontainer run fine on it, so we might as well just stop requesting it
on older versions of systemd which allowed it. (Those versions never really
changed behavior significantly when that option was passed anyways.)

Signed-off-by: Filipe Brandenburger <filbranden@gmail.com>
---
 libcontainer/cgroups/systemd/apply_systemd.go | 79 +------------------
 1 file changed, 4 insertions(+), 75 deletions(-)

diff --git a/libcontainer/cgroups/systemd/apply_systemd.go b/libcontainer/cgroups/systemd/apply_systemd.go
index a10e3f6a..15326603 100644
--- a/libcontainer/cgroups/systemd/apply_systemd.go
+++ b/libcontainer/cgroups/systemd/apply_systemd.go
@@ -72,11 +72,8 @@ const (
 )
 
 var (
-	connLock                   sync.Mutex
-	theConn                    *systemdDbus.Conn
-	hasStartTransientUnit      bool
-	hasStartTransientSliceUnit bool
-	hasDelegateSlice           bool
+	connLock sync.Mutex
+	theConn  *systemdDbus.Conn
 )
 
 func newProp(name string, units interface{}) systemdDbus.Property {
@@ -100,67 +97,8 @@ func UseSystemd() bool {
 		if err != nil {
 			return false
 		}
-
-		// Assume we have StartTransientUnit
-		hasStartTransientUnit = true
-
-		// But if we get UnknownMethod error we don't
-		if _, err := theConn.StartTransientUnit("test.scope", "invalid", nil, nil); err != nil {
-			if dbusError, ok := err.(dbus.Error); ok {
-				if dbusError.Name == "org.freedesktop.DBus.Error.UnknownMethod" {
-					hasStartTransientUnit = false
-					return hasStartTransientUnit
-				}
-			}
-		}
-
-		// Assume we have the ability to start a transient unit as a slice
-		// This was broken until systemd v229, but has been back-ported on RHEL environments >= 219
-		// For details, see: https://bugzilla.redhat.com/show_bug.cgi?id=1370299
-		hasStartTransientSliceUnit = true
-
-		// To ensure simple clean-up, we create a slice off the root with no hierarchy
-		slice := fmt.Sprintf("libcontainer_%d_systemd_test_default.slice", os.Getpid())
-		if _, err := theConn.StartTransientUnit(slice, "replace", nil, nil); err != nil {
-			if _, ok := err.(dbus.Error); ok {
-				hasStartTransientSliceUnit = false
-			}
-		}
-
-		for i := 0; i <= testSliceWait; i++ {
-			if _, err := theConn.StopUnit(slice, "replace", nil); err != nil {
-				if dbusError, ok := err.(dbus.Error); ok {
-					if strings.Contains(dbusError.Name, "org.freedesktop.systemd1.NoSuchUnit") {
-						hasStartTransientSliceUnit = false
-						break
-					}
-				}
-			} else {
-				break
-			}
-			time.Sleep(time.Millisecond)
-		}
-
-		// Not critical because of the stop unit logic above.
-		theConn.StopUnit(slice, "replace", nil)
-
-		// Assume StartTransientUnit on a slice allows Delegate
-		hasDelegateSlice = true
-		dlSlice := newProp("Delegate", true)
-		if _, err := theConn.StartTransientUnit(slice, "replace", []systemdDbus.Property{dlSlice}, nil); err != nil {
-			if dbusError, ok := err.(dbus.Error); ok {
-				// Starting with systemd v237, Delegate is not even a property of slices anymore,
-				// so the D-Bus call fails with "InvalidArgs" error.
-				if strings.Contains(dbusError.Name, "org.freedesktop.DBus.Error.PropertyReadOnly") || strings.Contains(dbusError.Name, "org.freedesktop.DBus.Error.InvalidArgs") {
-					hasDelegateSlice = false
-				}
-			}
-		}
-
-		// Not critical because of the stop unit logic above.
-		theConn.StopUnit(slice, "replace", nil)
 	}
-	return hasStartTransientUnit
+	return true
 }
 
 func (m *Manager) Apply(pid int) error {
@@ -196,10 +134,6 @@ func (m *Manager) Apply(pid int) error {
 
 	// if we create a slice, the parent is defined via a Wants=
 	if strings.HasSuffix(unitName, ".slice") {
-		// This was broken until systemd v229, but has been back-ported on RHEL environments >= 219
-		if !hasStartTransientSliceUnit {
-			return fmt.Errorf("systemd version does not support ability to start a slice as transient unit")
-		}
 		properties = append(properties, systemdDbus.PropWants(slice))
 	} else {
 		// otherwise, we use Slice=
@@ -212,12 +146,7 @@ func (m *Manager) Apply(pid int) error {
 	}
 
 	// Check if we can delegate. This is only supported on systemd versions 218 and above.
-	if strings.HasSuffix(unitName, ".slice") {
-		if hasDelegateSlice {
-			// systemd 237 and above no longer allows delegation on a slice
-			properties = append(properties, newProp("Delegate", true))
-		}
-	} else {
+	if !strings.HasSuffix(unitName, ".slice") {
 		// Assume scopes always support delegation.
 		properties = append(properties, newProp("Delegate", true))
 	}
-- 
2.23.0.351.gc4317032e6-goog

